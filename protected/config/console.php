<?php

// This is the configuration for yiic console application.
// Any writable CConsoleApplication properties can be configured here.

if (file_exists($dbConfigFilename = dirname(__FILE__) . DIRECTORY_SEPARATOR . '_db.php'))
{
	$dbConfig = require_once($dbConfigFilename);
}
else
{
	$dbConfig = array();
}

return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'My Console Application',

	// preloading 'log' component
	'preload'=>array('log'),

	// application components
	'components'=>array(
		'db' => CMap::mergeArray(
			array(
				'connectionString' => 'mysql:host=localhost;dbname=design_group;port=8889;unix_socket=/Applications/MAMP/tmp/mysql/mysql.sock',
				'emulatePrepare' => true,
				'username' => 'root',
				'password' => 'root',
				'charset' => 'utf8',				
			),
			$dbConfig
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
			),
		),
	),
);