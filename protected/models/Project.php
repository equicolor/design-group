<?php

/**
 * This is the model class for table "project".
 *
 * The followings are the available columns in table 'project':
 * @property integer $id
 * @property integer $customerId
 * @property integer $categoryId
 * @property string $name
 * @property string $content
 * @property string $team
 *
 * The followings are the available model relations:
 * @property Category $category
 * @property Customer $customer
 * @property Service[] $services
 */
class Project extends ActiveRecord
{
	public $team;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'project';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('customerId, categoryId, name, content, urlName', 'required'),
			array('urlName', 'unique'),
			array('urlName', 'length', 'max'=>32),
			array('customerId, categoryId', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>256),
			array('name', 'filter', 'filter' => array($this, 'multilineName')),
			array('team', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, customerId, categoryId, name, content, team', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'category' => array(self::BELONGS_TO, 'Category', 'categoryId'),
			'customer' => array(self::BELONGS_TO, 'Customer', 'customerId'),
			'services' => array(self::MANY_MANY, 'Service', 'serviceProject(projectId, serviceId)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'customerId' => 'Клиент',
			'categoryId' => 'Категория',
			'name' => 'Название',
			'content' => 'Контент',
			'team' => 'Комманда',
			'category.name' => 'Категория',
			'customer.name' => 'Клиент',
			'image' => 'Лого',
			'urlName' => 'Url-псевдоним',
		);
	}

	public function behaviors()
	{
		return array(
			array(
				'class' => 'JSONParamsBehavior',
				'listAttribute' => 'team',
				'jsonAttribute' => 'jsonTeam',
				'params' => array(
					'name', 'value',
				),
			),
			array(
				'class' => 'UploadFileBehavior',
				'attributeNames' => array(
					'image' => array(
						'types' => array('image'),
						'image' => true,
					),
				),
				'thumbSizes' => array(
					'default' => array(470, 240),
					'new' => array(450, 240),
				),
			),
			array(
				'class' => 'SortableArBehavior',
				// 'compareAttribute' => '',
				'positionAttribute' => 'position',
			),
		);
	}

	public function defaultScope()
	{
		$t = $this->getTableAlias(false, false);
		return array(
			'order' => "$t.position ASC"
		);
	}

	public function scopes()
	{
		$t = $this->tableAlias;
	
		return array(
			'new' => array(
				'join' => "RIGHT JOIN `newProject` ON $t.id = newProject.id",
			),
		);
	}

	public function byCategory($urlName)
	{
		if (!empty($urlName))
		{
			$this->dbCriteria->with = array(
				'category',
			);

			$this->dbCriteria->addCondition('category.urlName = :categoryUrlName');
			$this->dbCriteria->params[':categoryUrlName'] = $urlName;
		}

		return $this;
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('customerId',$this->customerId);
		$criteria->compare('categoryId',$this->categoryId);
		$criteria->compare('name',$this->name,true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'pagination' => false,
		));
	}

	protected function afterFind()
	{
		parent::afterFind();

		$this->name = str_replace('<br>', "\n", $this->name);
	}

	public function multilineName($value)
	{
		return str_replace("\n", '<br>', trim($value));
	}

	public function getRawName()
	{
		return str_replace("\n", '<br>', $this->name);
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Project the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
