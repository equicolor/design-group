<?php
/* @var $this AdminController */

$this->breadcrumbs=array(
	'Admin',
);

$this->pageTitle = 'Главная страница';
?>

<?php Yii::import('ext.imperavi-redactor-widget.ImperaviRedactorWidget'); ?>

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'customer-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="help-block">Поля с <span class="required">*</span> обязательны.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php
	/*
	<div class="control-group<?= $model->text->hasErrors('content') ? ' error' : ''; ?>">
	    <?= $form->labelEx($model->text, 'content', array(
	    	'class' => 'control-label',
	    ));?>
	    <?php $this->widget('ImperaviRedactorWidget', array(
	        'model' => $model->text,
	        'attribute' => 'content',
	        'options' => array(
	            'lang' => 'ru',
	            'imageUpload' => Yii::app()->createUrl('admin/main/imageUpload') . '/',
	            'convertVideoLinks' => true,
	            'minHeight' => 300
	        ),
	    )); ?>
	    <?= $form->error($model->text, 'content'); ?>
	</div>
	*/
	?>
	<?=	$form->dropDownListControlGroup($model, 'newProject1',
		CHtml::listData(Project::model()->findAll(), 'id', 'name'),
		array(
			'span' => 7,
			'empty' => '',
		)
	); ?>	

	<?=	$form->dropDownListControlGroup($model, 'newProject2',
		CHtml::listData(Project::model()->findAll(), 'id', 'name'),
		array(
			'empty' => '',
			'span' => 7,
		)
	); ?>	

	<?=	$form->dropDownListControlGroup($model, 'newProject3',
		CHtml::listData(Project::model()->findAll(), 'id', 'name'),
		array(
			'empty' => '',
			'span' => 7,
		)
	); ?>	

	<?=	$form->dropDownListControlGroup($model, 'newProject4',
		CHtml::listData(Project::model()->findAll(), 'id', 'name'),
		array(
			'empty' => '',
			'span' => 7,
		)
	); ?>	

	<div class="form-actions">
		<?=	TbHtml::submitButton('Сохранить', array(
			'color' => TbHtml::BUTTON_COLOR_PRIMARY,
			'size' => TbHtml::BUTTON_SIZE_DEFAULT,
		)); ?>
	</div>

<?php $this->endWidget(); ?>
