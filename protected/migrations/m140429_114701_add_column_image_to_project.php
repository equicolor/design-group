<?php

class m140429_114701_add_column_image_to_project extends CDbMigration
{
	public function up()
	{
		$this->addColumn('project', 'image', 'string');
	}

	public function down()
	{
		$this->dropColumn('project', 'image');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}