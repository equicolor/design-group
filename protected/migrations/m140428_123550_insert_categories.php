<?php

class m140428_123550_insert_categories extends CDbMigration
{
	public $rows = array(
		array(
			'name' => 'Айдентика',
			'urlName' => 'identity',
		),
		array(
			'name' => 'Полиграфия',
			'urlName' => 'polygraphy',
		),
		array(
			'name' => 'E-mail-рассылки',
			'urlName' => 'email',
		),
		array(
			'name' => 'Сайты',
			'urlName' => 'sites',
		),
		array(
			'name' => 'Оформление сообществ',
			'urlName' => 'communities',
		),
	);

	public function safeUp()
	{
		foreach ($this->rows as $row)
		{
			$this->insert('category', $row);
		}
	}

	public function safeDown()
	{
		foreach ($this->rows as $row)
		{
			$this->delete('category', "urlName = '{$row['urlName']}'");
		}
	}
	
}