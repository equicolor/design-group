<?php

class m140429_105352_init_selectedCustomer extends CDbMigration
{
	public function up()
	{
		$this->createTable('selectedCustomer', array(
			'id' => 'int(11) NOT NULL',
		));
	}

	public function down()
	{
		$this->dropTable('selectedCustomer');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}