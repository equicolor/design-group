<?php
/* @var $this CustomerController */

$this->breadcrumbs=array(
	'Customer'=>array('/customer'),
	'View',
);
?>

<div class="b-content">
	<div class="b-client--imageframe">
		<?= CHtml::image($model->image->thumb('menu')); ?>
	</div>

	<div class="b-client--text">
		<h3><?= $model->name; ?></h3>
		<?= CHtml::link($model->link, $model->link); ?>
		<?= $model->content; ?>
	</div>

	<div style="clear: both;"></div>
</div>
<div class="b-recentworks">
	<span class="b-recentworks--title">реализованные проекты</span>

	<ul class="b-recentworks--list">
		<?php foreach ($model->projects as $project): ?>
			<li>
				<div style="padding-right: 20px;">
					<a href="<?= $this->createUrl('project/view', array('urlName' => $project->urlName)); ?>">
						<div class="b-recentworks--imagewrapper">
							<?= CHtml::image($project->image->thumb()); ?>
						</div>
						<p><?= $project->name; ?></p>
					</a>
				</div>
			</li>
		<?php endforeach; ?>
	</ul>
</div>