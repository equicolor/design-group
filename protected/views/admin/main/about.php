<?php
/* @var $this AdminController */

$this->breadcrumbs=array(
	'Admin',
);

$this->pageTitle = 'О компании';
?>

<?php Yii::import('ext.imperavi-redactor-widget.ImperaviRedactorWidget'); ?>

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'customer-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="help-block">Поля с <span class="required">*</span> обязательны.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="control-group<?= $model->text->hasErrors('content') ? ' error' : ''; ?>">
	    <?= $form->labelEx($model->text, 'content', array(
	    	'class' => 'control-label',
	    ));?>
	    <?php $this->widget('ImperaviRedactorWidget', array(
	        'model' => $model->text,
	        'attribute' => 'content',
	        'options' => array(
	            'lang' => 'ru',
	            'imageUpload' => Yii::app()->createUrl('admin/main/imageUpload') . '/',
	            'convertVideoLinks' => true,
	            'minHeight' => 300
	        ),
	    )); ?>
	    <?= $form->error($model->text, 'content'); ?>
	</div>
	<div class="control-group">
		<?= $form->labelEx($model, 'selectedCustomers') ?>
		<?= $form->listbox(
			$model, 
			'selectedCustomers', 
			CHtml::listData(Customer::model()->findAll(), 'id', 'name'),
			array(
				'multiple' => 'multiple',
				'style' => 'width: 500px; height: 200px;',
			)
		); ?>
	</div>

	<div class="form-actions">
		<?=	TbHtml::submitButton('Сохранить', array(
			'color' => TbHtml::BUTTON_COLOR_PRIMARY,
			'size' => TbHtml::BUTTON_SIZE_DEFAULT,
		)); ?>
	</div>

<?php $this->endWidget(); ?>
