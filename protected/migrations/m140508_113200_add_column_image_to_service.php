<?php

class m140508_113200_add_column_image_to_service extends CDbMigration
{
	public function up()
	{
		$this->addColumn('service', 'image', 'string');
	}

	public function down()
	{
		$this->dropColumn('service', 'image');
	}
}