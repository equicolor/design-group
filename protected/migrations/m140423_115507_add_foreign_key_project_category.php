<?php

class m140423_115507_add_foreign_key_project_category extends CDbMigration
{
	public function up()
	{
		$this->addForeignKey(
			'projectBelongsToCategory',
			'project',
			'categoryId',
			'category',
			'id',
			'cascade',
			'cascade'
		);
	}

	public function down()
	{
		$this->dropForeignKey('projectBelongsToCategory', 'project');
	}
}