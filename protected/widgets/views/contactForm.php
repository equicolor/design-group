<?php $form = $this->beginWidget('CActiveForm', array(
	'enableClientValidation' => true,
	'action' => $this->controller->createUrl('site/contact'),
	'clientOptions' => array(
		'validateOnSubmit' => true,
		'validateOnInput' => false,
		'hideErrorMessage' => true,
		'afterValidateAttribute' => new CJavaScriptExpression(
<<<'JS'
	function (form, attribute, data, hasError)
	{
		$attributeField = $('#' + attribute.inputID);
		if (hasError)
		{
			$attributeField.addClass('st-error');
		}	
		else
		{
			$attributeField.removeClass('st-error');
		}
	}
JS
	)),
)); ?>
<div class="b-adding--contactform">
	<span class="b-adding--contactform_nametitle">Ваше имя</span>
	<?= $form->textField($model, 'name', array(
		'class' => 'b-adding--contactform_name',
	)); ?>
	<?= $form->error($model, 'name'); ?>
	<!-- <input class="b-adding--contactform_name"> -->

	<span class="b-adding--contactform_mailtitle">Ваш email</span>
	<?= $form->textField($model, 'email', array(
		'class' => 'b-adding--contactform_mail',
	)); ?>
	<?= $form->error($model, 'email'); ?>
	<!-- <input class="b-adding--contactform_mail"> -->

	<span class="b-adding--contactform_messagetitle">Сообщение</span>
	<?= $form->textArea($model, 'body', array(
		'class' => 'b-adding--contactform_message',
	)); ?>
	<?= $form->error($model, 'body'); ?>
	<!-- <textarea class="b-adding--contactform_message"></textarea> -->

	<button class="b-adding--contactform_send" type="submit">отправить</button>

	<span class="b-adding--contactform_startext">Ваш запрос будет рассмотрен в течение 24 часов.</span>
</div>

<?php $this->endWidget(); ?>