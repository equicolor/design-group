<?php
class MainPage extends MMForm 
{
	/* unsafe submodels */
	
	/* safe submodels */
	
	public $text;

	/* unsafe submodel attributes */
	
	/* safe form input attributes */
	
	public $newProject1;
	public $newProject2;
	public $newProject3;
	public $newProject4;

	public function rules()
	{
		return array(
			array('newProject1, newProject2, newProject3, newProject4', 'safe'),
			// array('text', 'submodel'),
		);
	}
	
	public function attributeLabels()
	{
		return array(
			'newProject1' => 'Свежая работа №1',
			'newProject2' => 'Свежая работа №2',
			'newProject3' => 'Свежая работа №3',
			'newProject4' => 'Свежая работа №4',
		);
	}

	public function prepare($params)
	{
		$this->text = $params['text'];

		$projects = NewProject::model()->findAll();

		/* простите меня за это ((( */

		$this->newProject1 = isset($projects[0]) ? $projects[0]->id : null;
		$this->newProject2 = isset($projects[1]) ? $projects[1]->id : null;
		$this->newProject3 = isset($projects[2]) ? $projects[2]->id : null;
		$this->newProject4 = isset($projects[3]) ? $projects[3]->id : null;
	}
	
	protected function saveNewProject($id)
	{
		$model = new NewProject();
		$model->id = $id;

		return $model->save(false);
	}

	protected function saveNewProjects()
	{
		$projects = array(
			$this->newProject1,
			$this->newProject2,
			$this->newProject3,
			$this->newProject4,
		);
		
		$result = true;

		foreach ($projects as $project)
		{
			if (!empty($project))
			{
				$result &= $this->saveNewProject($project);
			}
		}

		return $result;
	}

	public function saveProcess($runValidation = true)
	{
		$result = true;
					
		NewProject::model()->deleteAll();

		$result &= $this->saveNewProjects();
		$result &= $this->text->save(false);
	
		return $result;
	}

	public function issetAttributes($request)
	{
		return isset($request['MainPage']);
	}
	
	public function safeAssign($request)
	{
		// $this->text->attributes = $request['Text'];

		if (isset($request['MainPage']))
			$this->attributes = $request['MainPage'];
	}
	
	public static function create($params, $className = __CLASS__)
	{
		return parent::create($params, $className);
	}

}
