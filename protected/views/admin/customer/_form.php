<?php
/* @var $this CustomerController */
/* @var $model Customer */
/* @var $form CActiveForm */
?>

<?php Yii::import('ext.imperavi-redactor-widget.ImperaviRedactorWidget'); ?>

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'customer-form',
	'enableAjaxValidation'=>false,
	'htmlOptions' => array(
		'enctype' => 'multipart/form-data'
	),
)); ?>

	<p class="help-block">Поля с <span class="required">*</span> обязательны.</p>


	<?php echo $form->errorSummary($model); ?>

	<?= $form->textFieldControlGroup($model, 'name', array('span' => 7, 'maxlength' => 255)); ?>
	<?= $form->textFieldControlGroup($model, 'link', array('span' => 7, 'maxlength' => 255)); ?>
	<?= $form->textFieldControlGroup($model, 'urlName', array('span' => 7, 'maxlength' => 255)); ?>
	<div class="control-group<?= $model->hasErrors('content') ? ' error' : ''; ?>">
	    <?= $form->labelEx($model, 'content', array(
	    	'class' => 'control-label',
	    ));?>
	    <?php $this->widget('ImperaviRedactorWidget', array(
	        'model' => $model,
	        'attribute' => 'content',
	        'options' => array(
	            'lang' => 'ru',
	            'imageUpload' => Yii::app()->createUrl('admin/main/imageUpload') . '/',
	            'convertVideoLinks' => true,
	            'minHeight' => 300
	        ),
	    )); ?>
	    <?= $form->error($model, 'content'); ?>
	</div>

	<div>
		<?= $form->fileFieldControlGroup($model, 'image', array('span' => 5)); ?>
		<?= isset($model->image) ? TbHtml::imagePolaroid($model->image) : '' ?>
	</div>

	<div class="form-actions">
		<?=	TbHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', array(
			'color' => TbHtml::BUTTON_COLOR_PRIMARY,
			'size' => TbHtml::BUTTON_SIZE_DEFAULT,
		)); ?>
	</div>

<?php $this->endWidget(); ?>
