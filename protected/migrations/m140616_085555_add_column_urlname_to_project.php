<?php

class m140616_085555_add_column_urlname_to_project extends CDbMigration
{
	public function up()
	{
		$this->addColumn('project', 'urlName', 'varchar(32)');
	}

	public function down()
	{
		$this->dropColumn('project', 'urlName');
	}
}