<?php

class MainController extends AdminController
{
	public function actionIndex()
	{
		$text = Text::model()->findByPk('mainPage');

		$model = MainPage::create(array(
			'text' => $text,
		));

		if ($model->issetAttributes($_POST))
		{
			$model->safeAssign($_POST);

			if ($model->save())
			{
				Yii::app()->user->setFlash('result', 'Изменения успешно сохранены');
				$this->refresh();				
			}
		}

		$this->render('index', array(
			'model' => $model,
		));
	}

	public function actionAbout()
	{
		$text = Text::model()->findByPk('aboutPage');

		$model = AboutPage::create(array(
			'text' => $text,
		));

		if ($model->issetAttributes($_POST))
		{
			$model->safeAssign($_POST);
			
			if (!isset($_POST['AboutPage']['selectedCustomers']))
			{
				$model->selectedCustomers = array();
			}

			if ($model->save())
			{
				Yii::app()->user->setFlash('result', 'Изменения успешно сохранены');
				$this->refresh();				
			}
		}

		$this->render('about', array(
			'model' => $model,
		));
	}

	public function actionImageUpload()
	{
		$dir = Yii::app()->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'redactor' . DIRECTORY_SEPARATOR;
		
		if (!is_dir($dir))
		{
			if (!mkdir($dir, 755, true))
				throw new CHttpException(500, 'Unable to create directory');
		}

		if (isset($_FILES['file']['type']))
		{
			$_FILES['file']['type'] = strtolower($_FILES['file']['type']);

			if (
				$_FILES['file']['type'] == 'image/png'
			 || $_FILES['file']['type'] == 'image/jpg'
			 || $_FILES['file']['type'] == 'image/gif'
			 || $_FILES['file']['type'] == 'image/jpeg'
			 || $_FILES['file']['type'] == 'image/pjpeg'
			)
			{
				// setting file's mysterious name
				$filename = md5(date('YmdHis')).'.jpg';

				if (!is_dir($dir))
				{
					mkdir($dir);
				}
				
				// copying
				move_uploaded_file($_FILES['file']['tmp_name'], $dir . $filename);

				// displaying file
				$array = array(
					'filelink' => 'uploads/redactor/' . $filename
				);

				echo stripslashes(json_encode($array));
			}	
		}
		else
			throw new CHttpException(400);
	}
}