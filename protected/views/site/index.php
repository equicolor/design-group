
	<div class="b-content">

		<div class="fotorama" data-width="100%" data-ratio="940/265" data-fit="cover">
			<div data-img="images/02.png">
				<div class="slider--right">
					<h3 class="fotorama__select">ДИЗАЙН И ВЕРСТКА СТУДЕНЧЕСКОЙ ГАЗЕТЫ «ГОСТ»</h3>
					<span class="slider__text fotorama__select">«ГОСт» — газета профкома студентов Иркутского государственного университета. И первая в своем роде неформальная и многотиражная студенческая газета в городе.</span>
					<a href="http://creative-jm.ru/projects/gost" class="slider__link">Смотреть работу</a>
				</div>
			</div>

			<div data-img="images/01.png">
				<div class="slider--right">
					<h3 class="fotorama__select">КОРПОРАТИВНЫЙ ЖУРНАЛ «ИНТЕРНЕТ ГИД»</h3>
					<span class="slider__text fotorama__select">В креативной группе для «Телеос-1» придумали концепцию и разработали дизайн нового медиа под названием «Интернет Гид».</span>
					<a href="http://creative-jm.ru/projects/internet-guide" class="slider__link">Смотреть работу</a>
				</div>
			</div>

			<div data-img="images/03.png">
				<div class="slider--right">
					<h3 class="fotorama__select">ЛОГОТИП И ФУНКЦИОНАЛЬНЫЙ БРЕНДИНГ ДЛЯ «ТЕЛЕОС-1»</h3>
					<span class="slider__text fotorama__select">«Телеос-1» — крупнейший мультисервисный провайдер в городе Братске. В креативной группе для провайдера был разработан «косметический» редизайн логотипа.</span>
					<a href="http://creative-jm.ru/projects/teleos-branding" class="slider__link">Смотреть работу</a>
				</div>
			</div>
		</div>

		<ul class="b-uslugi--mainlist">
			<li>
				<a href="#">
					<i class="s s--uslugi_issl"></i>
					<p class="b-uslugi--mainlist_title">Полиграфическая верстка</p>
					<p class="b-uslugi--mainlist_text">С большой любовью придумываем и верстаем медиа для классных проектов</p>
				</a>
			</li>

			<li>
				<a href="#">
					<i class="s s--uslugi_design"></i>
					<p class="b-uslugi--mainlist_title">Email-маркетинг</p>
					<p class="b-uslugi--mainlist_text">Проектируем и разрабатываем интересные email-рассылки, которые не кидают в спам</p>
				</a>
			</li>

			<li>
				<a href="#">
					<i class="s s--uslugi_prodv"></i>
					<p class="b-uslugi--mainlist_title">Создание иллюстраций</p>
					<p class="b-uslugi--mainlist_text">Решаем задачи клиентов с помощью эффектных авторских иллюстраций</p>
				</a>
			</li>

			<li>
				<a href="#">
					<i class="s s--uslugi_product"></i>
					<p class="b-uslugi--mainlist_title">Диджитал-брендинг</p>
					<p class="b-uslugi--mainlist_text">Культивируем и выращиваем свежие идеи для брендов в диджитал-среде</p>
				</a>
			</li>
		</ul>

	</div>

	<?php $this->widget('widgets.NewProjectsWidget'); ?>

	<div class="b-adding">
		<div class="b-adding--leftad">
			<img src="images/ipad.png">

			<div class="b-adding--leftad_textbox">
				<h3>с ума сшедшие</h3>
				<a href="#">Журнал «Интернет Гид»</a>
				<p>«Интернет Гид» делится с чита­телями новостями виртуального мира, знакомит с интересными людь­ми, полезными сервисами, свежими играми, книгами, попу­лярным видео и многим другим.</p>
			</div>
		</div>

		<?php $this->widget('widgets.ContactFormWidget'); ?>

		<div style="clear: both;"></div>
	</div>
