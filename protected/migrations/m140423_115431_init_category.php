<?php

class m140423_115431_init_category extends CDbMigration
{
	public function up()
	{
		$this->createTable('category', array(
			'id' => 'pk',
			'name' => 'varchar(256) NOT NULL',
			'urlName' => 'varchar(32) NOT NULL',	
		));
	}

	public function down()
	{
		$this->dropTable('category');
	}
}