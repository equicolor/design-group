<div class="b-content">
	
	<div class="b-content--main">
		<?= $text->content; ?>
	</div>

	<div class="b-widget">
		<h3 style="padding-bottom: 10px">Клиенты design group jm</h3>

		<ul class="b-widget--clients">
		<?php foreach ($customers as $customer): ?>
			<li>
				<a class="b-widget--clients_link" href="<?= $this->createUrl('customer/view', array('urlName' => $customer->urlName)); ?>">
					<div class="b-widget--clients_imagewrapper">
						<?= CHtml::image($customer->image->thumb('menu')); ?>
					</div>
				</a>
			</li>
		<?php endforeach; ?>
		</ul>

		<h3 style="padding-top: 20px">Напишите нам</h3>
		<h3 style="padding-bottom: 20px">мы с радостью ответим</h3>

		<?php $this->widget('widgets.ContactFormWidget'); ?>
	</div>

	<div style="clear: both;"></div>
</div>

<?php $this->widget('widgets.NewProjectsWidget'); ?>