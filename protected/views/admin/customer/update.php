<?php
/* @var $this CustomerController */
/* @var $model Customer */

$this->breadcrumbs=array(
	'Клиенты'=>array('admin'),
	$model->name,
	'Редактирование',
);

$this->menu=array(
	array('label'=>'Добавить клиента', 'url'=>array('create')),
	array('label'=>'Список клиентов', 'url'=>array('admin')),
);

$this->pageTitle = "Редактирование клиента {$model->name}";
?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>