<?php
Yii::import('zii.widgets.CMenu');
// убрал циклические ссылки
class MainMenu extends CMenu
{
	protected function normalizeItems($items,$route,&$active)
	{
	    foreach($items as $i=>$item)
	    {
	        if(isset($item['visible']) && !$item['visible'])
	        {
	            unset($items[$i]);
	            continue;
	        }
	        if(!isset($item['label']))
	            $item['label']='';
	        if($this->encodeLabel)
	            $items[$i]['label']=CHtml::encode($item['label']);
	        $hasActiveChild=false;
	        if(isset($item['items']))
	        {
	            $items[$i]['items']=$this->normalizeItems($item['items'],$route,$hasActiveChild);
	            if(empty($items[$i]['items']) && $this->hideEmptyItems)
	            {
	                unset($items[$i]['items']);
	                if(!isset($item['url']))
	                {
	                    unset($items[$i]);
	                    continue;
	                }
	            }
	        }
	        if(!isset($item['active']))
	        {
	        	// var_dump($item);
	            if($this->activateParents && $hasActiveChild || $this->activateItems && $this->isItemActive($item,$route))
	            {
	                $active=$items[$i]['active']=true;
	            	$items[$i]['url']=null;
	            }
	            else
	            {	
	                $items[$i]['active']=false;
	            }
	        }
	        elseif($item['active'])
	        {

            	$items[$i]['url']=null;
	            $active=true;
	        }
	    }
	    // die;
	    return array_values($items);
	}

	protected function renderMenuItem($item)
	{
	    if(isset($item['url']))
	    {
	        $label=$this->linkLabelWrapper===null ? $item['label'] : CHtml::tag($this->linkLabelWrapper, $this->linkLabelWrapperHtmlOptions, $item['label']);
	        return CHtml::link($label,$item['url'],isset($item['linkOptions']) ? $item['linkOptions'] : array());
	    }
	    else
	        return CHtml::tag('a',isset($item['linkOptions']) ? $item['linkOptions'] : array(), $item['label']);
	}
}