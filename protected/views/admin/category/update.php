<?php
/* @var $this CustomerController */
/* @var $model Customer */

$this->breadcrumbs=array(
	'Категории'=>array('admin'),
	$model->name,
	'Редактирование',
);

$this->menu=array(
	array('label'=>'Добавить категорию', 'url'=>array('create')),
	array('label'=>'Список категорий', 'url'=>array('admin')),
);

$this->pageTitle = "Редактирование категории {$model->name}";
?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>