	<div class="b-recentworks">
		<span class="b-recentworks--title">свежие работы</span>

		<ul class="b-recentworks--list">
		<?php foreach ($projects as $project): ?>
			<li>
				<div style="padding-right: 20px;">
					<a href="<?= $this->controller->createUrl('project/view', array('urlName' => $project->urlName)); ?>">
						<div class="b-recentworks--imagewrapper">
							<?= CHtml::image($project->image->thumb()); ?>
						</div>
						<p><?= $project->name; ?></p>
					</a>
				</div>
			</li>
		<?php endforeach; ?>
		</ul>
	</div>