<?php
/* @var $this CustomerController */
/* @var $model Customer */

$this->breadcrumbs=array(
	'Клиенты'=>array('admin'),
	'Создать',
);

$this->menu=array(
	array('label'=>'Список клиентов', 'url'=>array('admin')),
);

$this->pageTitle = 'Добавить клиента';
?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>