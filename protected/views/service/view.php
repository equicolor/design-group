<div class="b-innernav">
	<?php $this->widget('widgets.ServiceMenu',array(
		'service' => $model,
		'services' => $this->services,
		'activeCssClass' => 'active_link',
		'htmlOptions' => array(
			'class' => 'b-innernav--menu',
		),
	)); ?>
</div>

<div class="b-content">
	<img class="b-content--spimage" src="<?= $model->image; ?>">

	<div class="b-client--text">
		<?= $model->content; ?>		
	</div>
</div>

<?php if (!empty($projects)): ?>

<div class="b-recentworks">
	<span class="b-recentworks--title">Примеры работ</span>
	<ul class="b-recentworks--list">
	<?php foreach($projects as $project): ?>
		<li>
			<div style="padding-right: 20px;">
				<a href="<?= $this->createUrl('project/view', array('urlName' => $project->urlName)); ?>">
					<div class="b-recentworks--imagewrapper">
						<?= CHtml::image($project->image->thumb()); ?>
					</div>
					<p><?= $project->name; ?></p>
				</a>
			</div>
		</li>
	<?php endforeach; ?>
	</ul>
</div>

<?php endif; ?>