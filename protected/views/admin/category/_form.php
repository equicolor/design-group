<?php
/* @var $this CustomerController */
/* @var $model Customer */
/* @var $form CActiveForm */
?>

<?php Yii::import('ext.imperavi-redactor-widget.ImperaviRedactorWidget'); ?>

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'customer-form',
	'enableAjaxValidation'=>false,
	'htmlOptions' => array(
		'enctype' => 'multipart/form-data'
	),
)); ?>

	<p class="help-block">Поля с <span class="required">*</span> обязательны.</p>

	<?php echo $form->errorSummary($model); ?>

	<?= $form->textFieldControlGroup($model, 'name', array('span' => 7, 'maxlength' => 255)); ?>
	<?= $form->textFieldControlGroup($model, 'urlName', array('span' => 7, 'maxlength' => 255)); ?>

	<div class="form-actions">
		<?=	TbHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', array(
			'color' => TbHtml::BUTTON_COLOR_PRIMARY,
			'size' => TbHtml::BUTTON_SIZE_DEFAULT,
		)); ?>
	</div>

<?php $this->endWidget(); ?>
