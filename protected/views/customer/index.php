<div class="b-content">
	<ul class="b-client--list">
		<?php foreach ($models as $model): ?>
			<li>
				<a href="<?= $this->createUrl('customer/view', array('urlName' => $model->urlName)); ?>">
					<div class="b-client--list_imagewrapper">
						<?= CHtml::image($model->image->thumb('menu')); ?>
					</div>
			
					<p class="b-client--list_link" style="text-align: center"><?= $model->name; ?></p>
				</a>
			</li>
		<?php endforeach; ?>
	</ul>
</div>
