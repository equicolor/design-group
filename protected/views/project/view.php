<div class="b-innernav">
	<?php $this->widget('widgets.CategoryMenu',array(
		'page' => 'view',
		'category' => $model->category,
		'categories' => $this->categories,		
		'activeCssClass' => 'active_link',
		'htmlOptions' => array(
			'class' => 'b-innernav--menu',
		),
	)); ?>
</div>

<div class="b-content">
	<!-- TODO: стилизовать этот заголовок -->
	<h3><?= $model->rawName; ?></h3>

	<div class="b-content--main">
		<?= $model->content; ?>
	</div>

	<div class="b-widget">
		<div class="b-widget--about">
			<b><?= Yii::t('customer', 'Клиент'); ?>:</b>
			<span class="b-widget--about_text">
				<?= CHtml::link(
					$model->customer->name, 
					array('customer/view', 'urlName' => $model->customer->urlName)
				); ?></span>
		</div>

		<?php foreach ($model->team as $member): ?>
			<?php if ($member['name'] != ''): ?>
				<div class="b-widget--about">
					<b><?= $member['name']; ?>:</b>
					<span class="b-widget--about_text"><?= $member['value']; ?></span>
				</div>
			<?php endif; ?>	
		<?php endforeach; ?>
		
	</div>
	<div style="clear: both;"></div>
</div>
