<?php
/* @var $this ProjectController */
/* @var $model Project */
/* @var $form CActiveForm */
?>

<?php Yii::import('ext.imperavi-redactor-widget.ImperaviRedactorWidget'); ?>

<?php

Yii::app()->clientScript->registerScript('itemParams', 

<<<JS

	$('#addParam').click(function() {
		$('p.itemParam:last').clone().insertAfter('p.itemParam:last');
		
		name = $('p.itemParam:last input').attr('name');
		index = /\d+/.exec(name)[0];
		index++;
		$('p.itemParam:last input').each(function () {
			$(this).attr('name', $(this).attr('name').replace(/\d+/, index));
			$(this).attr('value', '');
		});
		
		$('a.deleteParam').each(function () {
			$(this).show();
		});
		
	});
	

	$('a.deleteParam').live('click', function() {
		$(this).parent('p.itemParam').remove();
		
		if ($('p.itemParam').index() === 0)
		{
			$('a.deleteParam').hide();
		}
		
		return false;
	});
	
	
	if ($('p.itemParam').index() > 0)
	{
		$('a.deleteParam').each(function () {
			$(this).show();
		});	
	}
JS

);

?>

<div class="form">
	<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
		'id' => 'item-form',
		'enableAjaxValidation' => false,
		'htmlOptions' => array('enctype' => 'multipart/form-data'),
	)); ?>

	<p class="help-block">Поля с <span class="required">*</span> обязательны.</p>

	<?php echo $form->errorSummary($model); ?>
	
	<ul class="nav nav-tabs">
		<li class="active"><a href="#general" data-toggle="tab">Основное</a></li>
		<li><a href="#params" data-toggle="tab">Комманда</a></li>
	</ul>
	
	<div class="tab-content">
		<div class="tab-pane active" id="general">
			<?=	
			$form->dropDownListControlGroup($model, 'customerId',
				CHtml::listData(Customer::model()->findAll(), 'id', 'name'),
				array(
					'span' => 7,
				)
			); ?>

			<?=	
			$form->dropDownListControlGroup($model, 'categoryId',
				CHtml::listData(Category::model()->findAll(), 'id', 'name'),
				array(
					'span' => 7,
				)
			); ?>

			<?= $form->textAreaControlGroup($model, 'name', array('span' => 7, 'maxlength' => 256)); ?>
			<?= $form->textFieldControlGroup($model, 'urlName', array('span' => 7, 'maxlength' => 256)); ?>
			
			<div class="control-group<?= $model->hasErrors('content') ? ' error' : ''; ?>">
			    <?= $form->labelEx($model, 'content', array(
			    	'class' => 'control-label',
			    ));?>
			    <?php $this->widget('ImperaviRedactorWidget', array(
			        'model' => $model,
			        'attribute' => 'content',
			        'options' => array(
			            'lang' => 'ru',
			            'imageUpload' => Yii::app()->createUrl('admin/main/imageUpload') . '/',
			            'convertVideoLinks' => true,
			            'minHeight' => 300
			        ),
			    )); ?>
			    <?= $form->error($model, 'content'); ?>
			</div>

			<div>
				<?= $form->fileFieldControlGroup($model, 'image', array('span' => 5)); ?>
				<?= isset($model->image) ? TbHtml::imagePolaroid($model->image) : '' ?>
			</div>
			
		</div>
		
		<div class="tab-pane" id="params">
			<div class="itemParams">
			<?php foreach ($model->team as $key => $param): ?>
				<p class="itemParam">
					<input class="span3 itemParamName" type="text" value="<?= $param['name'] ?>" name="Project[team][<?= $key ?>][name]" maxlength="255">
					<input class="span3 itemParamValue" type="text" value="<?= $param['value'] ?>" name="Project[team][<?= $key ?>][value]" maxlength="255">
					<a href="#" class="deleteParam" style="display: none">Удалить</a>
				</p>
			<?php endforeach; ?>
			</div>
			<?=	TbHtml::button('Добавить', array(
				'size' => TbHtml::BUTTON_SIZE_DEFAULT,
				'id' => 'addParam',
			)); ?>
		</div>					
	</div>
	<div class="form-actions">
		
		<?=	TbHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', array(
			'color' => TbHtml::BUTTON_COLOR_PRIMARY,
			'size' => TbHtml::BUTTON_SIZE_DEFAULT,
		)); ?>

	</div>
	
<?php $this->endWidget(); ?>
</div>