<?php

class CustomerController extends Controller
{
	public function actionIndex()
	{
		$models = Customer::model()->findAll();

		$this->render('index', array(
			'models' => $models,
		));
	}

	public function actionView($urlName)
	{
		$model = $this->loadModel($urlName);

		$this->render('view', array(
			'model' => $model,
		));
	}

	public function loadModel($urlName)
	{
		$model=Customer::model()->with('projects')->findByAttributes(array(
			'urlName' => $urlName,
		));

		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
}