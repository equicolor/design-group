<?php

class ServiceController extends Controller
{
	public $services = array();

	public function init()
	{
		$this->services = Service::model()->with('projects')->findAll(array(
			'index' => 'urlName',
		));
	}

	public function actionView($urlName = null)
	{
		if ($urlName === null)
		{
			$model = Service::model()->find();
		}
		else
		{
			if (isset($this->services[$urlName]))
			{
				$model = $this->services[$urlName];
			}
		}

		if ($model === null)
			throw new CHttpException(404, Yii::t('service', 'Указанной услуги не существует'));
		
		$this->render('view', array(
			'model' => $model,
			'projects' => $model->projects,
		));
	}
}