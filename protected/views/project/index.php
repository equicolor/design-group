<div class="b-innernav">
	<?php $this->widget('widgets.CategoryMenu',array(
		'page' => 'index',
		'category' => $category,
		'categories' => $categories,
		'activeCssClass' => 'active_link',
		'htmlOptions' => array(
			'class' => 'b-innernav--menu',
		),
	)); ?>
</div>

<div class="b-content">	
	<?php if (!empty($models)): ?>
		<ul class="b-client--list">
			<?php foreach ($models as $model): ?>
				<li>
					<a href="<?= $this->createUrl('project/view', array('urlName' => $model->urlName)); ?>">
						<div class="b-client--list_imagewrapper">
							<?= CHtml::image($model->image->thumb()); ?>
						</div>
						<p class="b-client--list_link"><?= $model->name; ?></p>
					</a>
				</li>
			<?php endforeach; ?>
		</ul>
	<?php else: ?>
		<?= Yii::t('project', 'В выбранной категории нет работ'); ?>
	<?php endif; ?>
</div>
