<?php
return array(
	'hookKey' => '4q7EWZzT3pbvJW7',
	'gitPath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '.git',
	'logFile' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'runtime' . DIRECTORY_SEPARATOR . 'deploy.log',
	'mailsTo' => array(
		'eqcolor@gmail.com',
		'sk00rd@gmail.com',
	),
	'mailFrom' => 'admin@test',
	
	'writeRequestsLog' => false,
	'echoOutput' => true,
);