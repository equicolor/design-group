<?php

class m140505_081605_add_column_position_to_service extends CDbMigration
{
	public function up()
	{
		$this->addColumn('service',  'position', 'int(11)');
	}

	public function down()
	{
		$this->dropColumn('service', 'position');
	}
}