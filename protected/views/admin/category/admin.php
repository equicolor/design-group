<?php
/* @var $this CustomerController */
/* @var $model Customer */

$this->breadcrumbs=array(
	'Категории'=>array('admin'),
	'Список',
);

$this->menu=array(
	array('label'=>'Добавить категорию', 'url'=>array('create')),
);

$this->pageTitle = 'Список категорий';
?>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'category-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'name',
		array(
			'header' => 'Количество работ',
			'value' => '$data->projectsCount',
		),
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'viewButtonUrl' => 'Yii::app()->createUrl(\'project/index\', array(\'urlName\' => $data->urlName))',
			'deleteConfirmation' => 'Вместе с категорией исчезнут все работы этой категории!!! Вы действительно хотите удалить категорию?',
		),
	),
)); ?>
