<?php
/* @var $this CustomerController */
/* @var $model Customer */

$this->breadcrumbs=array(
	'Категории'=>array('admin'),
	'Создать',
);

$this->menu=array(
	array('label'=>'Список категорий', 'url'=>array('admin')),
);

$this->pageTitle = 'Добавить категорию';
?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>