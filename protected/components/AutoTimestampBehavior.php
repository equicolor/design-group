<?php

class AutoTimestampBehavior extends CActiveRecordBehavior 
{
	public $createAttribute = 'createTime';
	public $updateAttribute = 'updateAttribute';

	// список сценариев, при которых поведение будет включено
	public $scenarios = null;
	
	public function attach($owner)
	{
		parent::attach($owner);
		
		if ($this->scenarios !== null && !is_array($this->scenarios))
		{
			throw new CException('Invalid value for scenarios in AutoTimestampBehavior');
		}
	}
	
	public function beforeValidate($on) 
	{
		if ($this->scenarios === null || in_array($this->owner->scenario, $this->scenarios))
		{
			$attribute = $this->Owner->isNewRecord ? $this->createAttribute : $this->updateAttribute;
			
			if ($attribute !== null)
			{
				$this->Owner->$attribute = time();
			}
		}
		
		return true;
	}
}