<?php

class m140429_100852_insert_text_items extends CDbMigration
{
	public $rows = array(
		array(
			'place' => 'mainPage',
			'content' => '',
			'name' => 'Контент на главной',
		),
		array(
			'place' => 'aboutPage',
			'content' => '',
			'name' => 'Контент на странице о компании',
		),
	);

	public function safeUp()
	{
		foreach ($this->rows as $row)
		{
			$this->insert('text', $row);
		}
	}

	public function safeDown()
	{
		foreach ($this->rows as $row)
		{
			$this->delete('text', "place = '{$row['place']}'");
		}
	}
}