<?php
/* @var $this ProjectController */
/* @var $model Project */

$this->breadcrumbs=array(
	'Работы'=>array('admin'),
	$model->name,
	'Редактирование',
);

$this->menu=array(
	array('label'=>'Создать работу', 'url'=>array('create')),
	array('label'=>'Список работ', 'url'=>array('admin')),
);
$this->pageTitle = "Редактирование работы #{$model->id}";
?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>