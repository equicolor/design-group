<?php
class ContactFormWidget extends CWidget
{
	public function run()
	{
		$model = new ContactForm;

		$this->render('contactForm', array(
			'model' => $model,
		));
	}
}