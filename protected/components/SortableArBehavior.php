<?
class SortableArBehavior extends CActiveRecordBehavior
{
	public $positionAttribute = 'position';
	public $compareAttribute = null;
	
	public function beforeSave($event)
	{
		if ($this->owner->isNewRecord) 
		{
			$t = $this->owner->tableName();
			$class = get_class($this->owner);
			$command = new CDbCommand(Yii::app()->db);
			$command->select("MAX($t.{$this->positionAttribute}) AS 'position'")->from("$t");
			
			if ($this->compareAttribute !== null)
			{
				$where = "$t.{$this->compareAttribute} = :{$this->compareAttribute}";
				$params = array(':' . $this->compareAttribute => $this->owner->{$this->compareAttribute});
				$command->where($where, $params);
			}
			
			$position = $command->query()->read();
			$this->owner->{$this->positionAttribute} = $position['position'] + 1;	
		}
		
		parent::beforeSave($event);
	}
}
