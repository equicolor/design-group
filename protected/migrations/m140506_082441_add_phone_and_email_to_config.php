<?php

class m140506_082441_add_phone_and_email_to_config extends CDbMigration
{
	public $table = 'config';

	public $rows = array(
		array(
			'id' => 'phone',
			'key' => 'Телефон',
			'value' => '+7 (495) 123 45 67',
			'is_hidden' => 0,
			'is_file' => 0,
		),
		array(
			'id' => 'email',
			'key' => 'E-mail',
			'value' => 'inbox@designgroup.ru',
			'is_hidden' => 0,
			'is_file' => 0,
		),
	);

	public function safeUp()
	{
		foreach ($this->rows as $row)
		{
			$this->insert($this->table, $row);
		}
	}

	public function safeDown()
	{
		foreach ($this->rows as $row)
		{
			$this->delete($this->table, "id = '{$row['id']}'");
		}
	}
}