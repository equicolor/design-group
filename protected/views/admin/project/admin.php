<?php
/* @var $this ProjectController */
/* @var $model Project */

$this->breadcrumbs=array(
	'Работы'=>array('admin'),
	'Список',
);

$this->menu=array(
	array('label'=>'Создать работу', 'url'=>array('create')),
);

$this->pageTitle = 'Список работ';
?>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'project-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'rowCssClassExpression'=>'"Item[]_{$data->id}"',
	'afterAjaxUpdate' => 'function(id, data){initSortable()	}',
	'columns'=>array(
		'id',
		array(
			'name' => 'customer.name',
			'filter' => CHtml::activeDropDownList(
				$model,
				'customerId',
				CHtml::listData(Customer::model()->findAll(), 'id', 'name'),
				array('empty' => 'Все')
			),
		),
		array(
			'name' => 'category.name',
			'filter' => CHtml::activeDropDownList(
				$model,
				'categoryId',
				CHtml::listData(Category::model()->findAll(), 'id', 'name'),
				array('empty' => 'Все')
			),
		),
		'name',
		'urlName',
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'viewButtonUrl' => 'Yii::app()->createUrl(\'project/view\', array(\'urlName\' => $data->urlName))',
		),
	),
)); ?>

<div class="alert alert-info">
	<button type="button" class="close" data-dismiss="alert">×</button>
	В таблице работает сортировка с помощью Drag&Drop.
</div>

<?php Yii::app()->clientScript->registerCoreScript('jquery.ui'); ?>
<?php Yii::app()->clientScript->registerScript('dragndrop_section_sorting', 
<<<JS
	$(function() {
		var fixHelper = function(e, ui) 
		{
            ui.children().each(
				function() 
				{
					$(this).width($(this).width());
				}
			);
			
			return ui;
        };
		
		initSortable = function () 
		{
			$( "#project-grid tbody").sortable({
				helper: fixHelper,
				update: function(event, ui) {
					var info = $(this).sortable("serialize", {
						key : 'Project[]',
						attribute: 'class',
					});
					
					$.ajax({
						type: "POST",
						url: "{$this->createUrl('sort')}",
						data: info,
						context: document.body,
					});
				},
			}).disableSelection();
		}
		
		initSortable();
	});
JS
); ?>