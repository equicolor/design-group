<?php
class ServiceForm extends MMForm 
{
	/* unsafe submodels */
	
	/* safe submodels */
	
	public $service;

	/* unsafe submodel attributes */
	
	/* safe form input attributes */
	
	public $projects = array();

	public function rules()
	{
		return array(
			array('projects', 'safe'),
			array('service', 'submodel'),
		);
	}
	
	public function attributeLabels()
	{
		return array(
			'projects' => 'Примеры работ',
		);
	}

	public function prepare($params)
	{
		if (isset($params['service']))
		{
			$this->service = $params['service'];

			$projects = ServiceProject::model()->findAllByAttributes(array(
				'serviceId' => $this->service->id,
			));

			foreach ($projects as $project)
			{
				$this->projects[] = $project->projectId;
			}
		}
		else
			$this->service = new Service;
	}
	
	protected function saveProject($id)
	{
		$model = new ServiceProject();
		$model->serviceId = $this->service->id;
		$model->projectId = $id;

		return $model->save(false);
	}

	protected function saveProjects()
	{
		$result = true;

		foreach ($this->projects as $project)
		{
			if (!empty($project))
			{
				$result &= $this->saveProject($project);
			}
		}

		return $result;
	}

	public function saveProcess($runValidation = true)
	{
		$result = true;
					

		$result &= $this->service->save(false);

		if ($result)
		{
			ServiceProject::model()->deleteAll('serviceId = :serviceId', array(':serviceId' => $this->service->id));
			$result &= $this->saveProjects();
		}
	
		return $result;
	}

	public function issetAttributes($request)
	{
		return isset($request['Service']);
	}
	
	public function safeAssign($request)
	{
		$this->service->attributes = $request['Service'];

		if (isset($request['ServiceForm']))
			$this->attributes = $request['ServiceForm'];
	}
	
	public static function create($params = array(), $className = __CLASS__)
	{
		return parent::create($params, $className);
	}

}
