<?php
class ServiceMenu extends CWidget
{
	public $service;
	public $services = array();
	public $htmlOptions = array();
	public $activeCssClass = 'active';

	public function run()
	{
		$items = array();

		foreach ($this->services as $service)
		{
			$items[] = array(
				'label' => $service->name,
				'url' => array('service/view', 'urlName' => $service->urlName),
				'active' => $service->id === $this->service->id,
			);
		}

		$this->controller->widget('zii.widgets.CMenu',array(
			'items' => $items,
			'htmlOptions' => $this->htmlOptions,
			'activeCssClass' => $this->activeCssClass,
		)); 
	}
}