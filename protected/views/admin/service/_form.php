<?php
/* @var $this ServiceController */
/* @var $model MMForm */
/* @var $form CActiveForm */
?>

<?php Yii::import('ext.imperavi-redactor-widget.ImperaviRedactorWidget'); ?>

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'service-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

	<p class="help-block">Поля с <span class="required">*</span> обязательны.</p>


	<?php echo $form->errorSummary($model); ?>

	<?= $form->textFieldControlGroup($model->service, 'name', array('span' => 7, 'maxlength' => 255)); ?>
	<?= $form->textFieldControlGroup($model->service, 'urlName', array('span' => 7, 'maxlength' => 255)); ?>

	<div class="control-group<?= $model->service->hasErrors('content') ? ' error' : ''; ?>">
	    <?= $form->labelEx($model->service, 'content', array(
	    	'class' => 'control-label',
	    ));?>
	    <?php $this->widget('ImperaviRedactorWidget', array(
	        'model' => $model->service,
	        'attribute' => 'content',
	        'options' => array(
	            'lang' => 'ru',
	            'imageUpload' => Yii::app()->createUrl('admin/main/imageUpload') . '/',
	            'convertVideoLinks' => true,
	            'minHeight' => 300
	        ),
	    )); ?>
	    <?= $form->error($model->service, 'content'); ?>
	</div>

	<div>
		<?= $form->fileFieldControlGroup($model->service, 'image', array('span' => 5)); ?>
		<?= isset($model->service->image) ? TbHtml::imagePolaroid($model->service->image) : '' ?>
	</div>
	
	<div class="control-group">
		<?= $form->labelEx($model, 'projects') ?>
		<?= 
		$form->listbox($model, 'projects', 
			CHtml::listData(
				Project::model()->findAll(),
				'id', 
				'name'
			), 
			array(
				'multiple' => 'multiple'
			)
		); ?>
	</div>

	<div class="form-actions">
		<?=	TbHtml::submitButton($model->service->isNewRecord ? 'Создать' : 'Сохранить', array(
			'color' => TbHtml::BUTTON_COLOR_PRIMARY,
			'size' => TbHtml::BUTTON_SIZE_DEFAULT,
		)); ?>
	</div>

<?php $this->endWidget(); ?>
