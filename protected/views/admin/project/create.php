<?php
/* @var $this ProjectController */
/* @var $model Project */

$this->breadcrumbs=array(
	'Работы'=>array('admin'),
	'Создать',
);

$this->menu=array(
	array('label'=>'Список работ', 'url'=>array('admin')),
);

$this->pageTitle = 'Добавить работу';
?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>