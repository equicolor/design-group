<?php

class m140423_115555_init_many_many_service_project extends CDbMigration
{
	public function up()
	{
		$this->createTable('serviceProject', array(
			'serviceId' => 'int(11) NOT NULL',
			'projectId' => 'int(11) NOT NULL',
			'PRIMARY KEY (serviceId, projectId)',
		));
	}

	public function down()
	{
		$this->dropTable('serviceProject');
	}
}