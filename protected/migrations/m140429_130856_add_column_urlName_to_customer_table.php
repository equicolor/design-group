<?php

class m140429_130856_add_column_urlName_to_customer_table extends CDbMigration
{
	public function up()
	{
		$this->addColumn('customer', 'urlName', 'string NOT NULL');
	}

	public function down()
	{
		$this->dropColumn('customer', 'urlName');
	}
	
	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}