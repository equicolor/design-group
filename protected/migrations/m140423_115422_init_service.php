<?php

class m140423_115422_init_service extends CDbMigration
{
	public function up()
	{
		$this->createTable('service', array(
			'id' => 'pk',
			'name' => 'varchar(256) NOT NULL',
			'urlName' => 'varchar(32) NOT NULL',
			'content' => 'MEDIUMTEXT NOT NULL',
		));
	}

	public function down()
	{
		$this->dropTable('service');
	}
}