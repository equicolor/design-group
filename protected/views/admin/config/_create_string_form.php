<?php
/* @var $this ConfigController */
/* @var $model Config */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'config-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

	<p class="help-block">Поля с <span class="required">*</span> обязательны.</p>

	<?php echo $form->errorSummary($model); ?>

	<?= $form->textFieldControlGroup($model, 'id', array('span' => 5, 'maxlength' => 16)); ?>
	
	<?= $form->textFieldControlGroup($model, 'key', array('span' => 5, 'maxlength' => 255)); ?>
	
	<?= $form->textFieldControlGroup($model, 'value', array('span' => 5, 'maxlength' => 255)); ?>
	
	<?= $form->hiddenField($model, 'is_file', array(
		'value' => 0,
	)); ?>

	<div class="form-actions">
		<?=	TbHtml::submitButton('Создать', array(
			'color' => TbHtml::BUTTON_COLOR_PRIMARY,
			'size' => TbHtml::BUTTON_SIZE_DEFAULT,
			'name' => 'apply',
		)); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->