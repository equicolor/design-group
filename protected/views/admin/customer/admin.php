<?php
/* @var $this CustomerController */
/* @var $model Customer */

$this->breadcrumbs=array(
	'Клиенты'=>array('admin'),
	'Список',
);

$this->menu=array(
	array('label'=>'Добавить клиента', 'url'=>array('create')),
);

$this->pageTitle = 'Список клиентов';
?>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'customer-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'name',
		'link',
		'urlName',
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'viewButtonUrl' => 'Yii::app()->createUrl(\'customer/view\', array(\'urlName\' => $data->urlName))',
		),
	),
)); ?>
