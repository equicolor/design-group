<?php
class NewProjectsWidget extends CWidget
{
	public function run()
	{
		$projects = Project::model()->new()->findAll();

		$this->render('newProjects', array(
			'projects' => $projects,
		));
	}
}