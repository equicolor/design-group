<?php

class SortAction extends CAction	
{
	public $className;
	public $positionAttribute;
	public $criteriaCallback;
	public $params = array();
	
	private $_currentParams = array();
	
	public function run()
	{		
		$this->checkRequestParams();
		
		try
		{
			$this->sort();
		}
		catch (CException $e)
		{
			throw new CHttpException(400);
		}
	}
	
	protected function sort()
	{
		if (isset($_POST[$this->className]))
		{
			$position = 0;
			foreach ($_POST[$this->className] as $pk)
			{
				$model = $this->loadModel($pk);
				
				if ($model !== null)
				{
					$model->{$this->positionAttribute} = $position++;
					$model->save(false);
				}
			}
		}
	}
	
	protected function checkRequestParams()
	{
		if (!empty($this->params) && is_array($this->params))
		{
			foreach ($this->params as $param)
			{
				$value = Yii::app()->request->getParam($param);
				
				if (null === $value)
				{
					throw new CHttpException(400);
				}
				else
				{
					$this->_currentParams[$param] = $value;
				}
			}
		}
	}
	
	protected function getRequestParams()
	{
		return $this->_currentParams;
	}
	
	protected function loadModel($pk)
	{
		$result = null;
		
		$class = $this->className;
		
		if (is_callable($this->criteriaCallback))
		{
			$criteria = new CDbCriteria(call_user_func(
				$this->criteriaCallback, 
				$pk, 
				$this->requestParams
			));
			
			$result = $class::model()->find($criteria);
		}
		else
		{
			$result = $class::model()->findByPk($pk);
		}
			
		return $result;
	}
}
