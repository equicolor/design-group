<?php

class m140423_115414_init_customer extends CDbMigration
{
	public function up()
	{
		$this->createTable('customer', array(
			'id' => 'pk',
			'name' => 'varchar(256) NOT NULL',
			'link' => 'string',
			'content' => 'MEDIUMTEXT NOT NULL',
			'image' => 'string',	
		));
	}

	public function down()
	{
		$this->dropTable('customer');
	}
}