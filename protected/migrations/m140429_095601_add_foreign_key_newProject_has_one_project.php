<?php

class m140429_095601_add_foreign_key_newProject_has_one_project extends CDbMigration
{
	public function up()
	{
		$this->addForeignKey(
			'newProjectHasOneProject',
			'newProject',
			'id',
			'project',
			'id',
			'cascade',
			'cascade'
		);
	}

	public function down()
	{
		$this->dropForeignKey('newProjectHasOneProject', 'newProject');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}