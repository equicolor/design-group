<?php

class m140423_115454_add_foreign_key_project_customer extends CDbMigration
{
	public function up()
	{
		$this->addForeignKey(
			'projectBelongsToCustomer',
			'project',
			'customerId',
			'customer',
			'id',
			'cascade',
			'cascade'
		);
	}

	public function down()
	{
		$this->dropForeignKey('projectBelongsToCustomer', 'project');
	}
}