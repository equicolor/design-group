<?php
class CategoryMenu extends CWidget
{
	public $page;
	public $category;
	public $categories = array();
	public $htmlOptions = array();
	public $activeCssClass = 'active';

	public function run()
	{
		$items = array(
			array(
				'label' => 'Все',
				'url' => array('project/index'),
				'active' => $this->category === null,
			),
		);

		if ($this->page == 'index')
		{
			foreach ($this->categories as $category)
			{
				$items[] = array(
					'label' => $category->name,
					'url' => array('project/index', 'urlName' => $category->urlName),
				);
			}
		}
		elseif ($this->page == 'view')
		{
			foreach ($this->categories as $category)
			{
				$items[] = array(
					'label' => $category->name,
					'url' => array('project/index', 'urlName' => $category->urlName),
					'active' => $category->id == $this->category->id,
				);
			}
		}

		$this->controller->widget('zii.widgets.CMenu',array(
			'items' => $items,
			'htmlOptions' => $this->htmlOptions,
			'activeCssClass' => $this->activeCssClass,
		)); 
	}
}