<?php
abstract class MMForm extends CFormModel 
{
	protected $_transaction;

	abstract public function saveProcess($runValidation);
	
	public static function create($params, $className)
	{
		$model = new $className;
		
		$model->prepare($params);
		
		return $model;
	}
	
	public function prepare($params)
	{
		
	}
	
	public function issetAttributes($request)
	{
		return isset($request[get_class($this)]);
	}
	
	public function safeAssign($request)
	{
		$this->attributes = $request[get_class($this)];
	}

	public function save($runValidation = true, $attributes = null)
	{
		if (!$runValidation || $this->validate($attributes))
		{
			if ($this->beforeSave())
			{
				$this->beginTransaction();
				
				$result = $this->saveProcess($runValidation);
				
				if ($result)
				{
					$this->afterSave();
				}
				
				$this->finishTransaction($result);
			}
		}
		else
			$result = false;
		
		return $result;
	}
	
	protected function beginTransaction()
	{
		if (Yii::app()->db->currentTransaction === null)
		{
			$this->_transaction = Yii::app()->db->beginTransaction();
		}
		else
			$this->_transaction = Yii::app()->db->currentTransaction;
	}
	
	protected function finishTransaction($commit = false)
	{
		if ($this->_transaction !== null)
		{
			if ($commit)
			{
				$this->_transaction->commit();
			}
			else
			{
				$this->_transaction->rollback();
			}
		}	
	}
	
	public function submodel($attribute, $params)
	{
		if ($this->$attribute instanceOf CModel)
		{
			if (!$this->$attribute->validate())
			{
				$this->addErrors($this->$attribute->errors);
			}
		}
	}
	
	public function onBeforeSave($event)
	{
		$this->raiseEvent('onBeforeSave',$event);
	}

	protected function beforeSave()
	{
		if($this->hasEventHandler('onBeforeSave'))
		{
			$event=new CModelEvent($this);
			$this->onBeforeSave($event);
			return $event->isValid;
		}
		else
			return true;
	}

	public function onAfterSave($event)
	{
		$this->raiseEvent('onAfterSave',$event);
	}

	protected function afterSave()
	{
		if($this->hasEventHandler('onAfterSave'))
			$this->onAfterSave(new CEvent($this));
	}

}
