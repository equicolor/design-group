<?php
/**
 * Created by PhpStorm.
 * User: danilzakablukovsky
 * Date: 08.11.13
 * Time: 14:02
 */

class AdminController extends Controller
{

	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout = '//layouts/admin/admin_column2';
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu = array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs = array();

	public function filters()
	{
		return array(
			'accessControl',
			'postOnly + delete',
		);
	}

	public function accessRules()
	{
		return array(
			// array(
			// 	'allow',
			// 	'roles' => array('admin'),
			// ),
			//todo: убрать правило с юзером admin, доступ должен быть по ролям
			array(
				'allow',
				'users' => array('admin'),
			),
			array(
				'allow',
				'actions' => array('login', 'error'),
				'users' => array('*'),
			),
			array(
				'deny',
				'users' => array('*'),
			),
		);
	}

	public function init()
	{
		Yii::app()->user->loginUrl = array('site/login');
	}
} 