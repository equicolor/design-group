<?php

class ProjectController extends Controller
{
	public $categories = array();

	public function init()
	{
		$this->categories = Category::model()->findAll(array(
			'index' => 'urlName',
		));
	}

	public function actionIndex($urlName = null)
	{
		$category = null;

		if ($urlName !== null)
		{
			if (isset($this->categories[$urlName]))
			{
				$category = $this->categories[$urlName];
			}
			else
				throw new CHttpException(404, Yii::t('project', 'Указанной категории не существует'));			
		}

		$models = Project::model()->byCategory($urlName)->findAll();

		$this->render('index', array(
			'models' => $models,
			'category' => $category,
			'categories' => $this->categories,
		));
	}

	public function actionView($urlName)
	{
		$model = Project::model()->findByAttributes(array(
			'urlName' => $urlName,
		));

		if ($model === null)
			throw new CHttpException(404);

		$this->render('view', array(
			'model' => $model,
			'categories' => $this->categories,
		));
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}