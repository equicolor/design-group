<?php

class m140423_115635_add_foreign_key_service_project_project extends CDbMigration
{
	public function up()
	{
		$this->addForeignKey(
			'projectHasManyService',
			'serviceProject',
			'projectId',
			'project',
			'id',
			'cascade',
			'cascade'
		);
	}

	public function down()
	{
		$this->dropForeignKey('projectHasManyService', 'serviceProject');
	}
}