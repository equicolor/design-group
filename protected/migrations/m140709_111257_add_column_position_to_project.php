<?php

class m140709_111257_add_column_position_to_project extends CDbMigration
{
	public function up()
	{
		$this->addColumn('project', 'position', 'int(11) NOT NULL');
	}

	public function down()
	{
		$this->dropColumn('project', 'position');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}