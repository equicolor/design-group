<?
/**
 * @author Danil Zakablukovskii danil.kabluk@gmail.com
 *
 * Поведение позволяет загружать и обрабатывать файлы для определенного атрибута
 * Class UploadFileBehavior
 */
class UploadFileBehavior extends CActiveRecordBehavior
{

	static $t = -1;

	/**
	 * @var array название атрибутов, хранящих в себе имя файла и файл:
	 * ключ - название атрибута, значение - массив со структурой:
	 * types - массив с нужными названиями типов расширений, который описываются в $this->fileTypes,
	 * image - булево, изображение или нет (нужно ли вместо атрибута ставить класс Image)
	 */
	public $attributeNames = array(
		'image' => array(
			'types' => array('image'),
			'image' => true,
		)
	);
	/**
	 * @var string алиас директории, куда будем сохранять файлы
	 */
	public $savePath = 'uploads';
	/**
	 * @var string типы файлов, которые можно загружать (нужно для валидации):
	 * ключ - название типа, значение - массив с разрешенными расширениями
	 */
	public $fileTypes = array(
		'image' => 'jpeg, jpg, png',
		'text_file' => 'pdf, doc, docx, txt, xls, xlsx, rtf',
	);
	/**
	 * @var array необходимые названия и размеры превью:
	 * ключ - название, значение - массив с шириной и высотой
	 */
	public $thumbSizes = array(
		'default' => array(200, 200),
	);

	public $saveOriginalFilename = false;

	/**
	 * @param CActiveRecord $owner
	 *
	 * @throws Exception
	 */
	public function attach($owner)
	{
		parent::attach($owner);

		$this->savePath .= '/' . strtolower(get_class($owner));

		foreach ($this->attributeNames as $attrName => $attrArray) {
			if (!$owner->hasAttribute($attrName))
				throw new Exception('Модель "' . get_class($owner) . '" не имеет атрибута "' . $attrName . '".');

			$types = $this->getFileExtensions($attrName);

			// if ($this->hasFakeUploadedFileInstance())
			// {
				$fileValidator = CValidator::createValidator('file', $owner, $attrName,
					array('types' => $types, 'allowEmpty' => true, 'safe' => false));
				$owner->validatorList->add($fileValidator);
			// }
		}
	}

	public function hasFakeUploadedFileInstance()
	{
		return isset($this->owner->fakeUploadedFileInstance) && $this->owner->fakeUploadedFileInstance !== null;
	}
	
	public function getUploadedFileInstance($attrName)
	{
		if ($this->hasFakeUploadedFileInstance())
		{
			return $this->owner->fakeUploadedFileInstance;
		}
		else
			return CUploadedFile::getInstance($this->owner, $attrName);
	}
	
	/**
	 * Возвращает строку с расширениями файла, который должны быть применены к атрибуту
	 * @param $attrName имя атрибута
	 *
	 * @return string строка с расширениями файла
	 * @throws InvalidArgumentException
	 * @throws Exception
	 */
	public function getFileExtensions($attrName)
	{
		if (!isset($this->attributeNames[$attrName]['types']) || !is_array($this->attributeNames[$attrName]['types']))
			throw new InvalidArgumentException("Для атрибута $attrName нужные типы не описаны или описаны неверно");

		$types = array();
		foreach ($this->attributeNames[$attrName]['types'] as $type) {
			if (!isset($this->fileTypes[$type]))
				throw new Exception("Тип $type не существует общем списке типов в \$this->fileTypes");

			$types[] = $this->fileTypes[$type];
		}

		return implode(',', $types);
	}

	public function beforeSave($event)
	{
		foreach ($this->attributeNames as $attrName => $attrArray) {
			// if ($file = CUploadedFile::getInstance($this->owner, $attrName)) {
			if ($file = $this->getUploadedFileInstance($attrName)) {
				$this->checkSavePath();
				$this->deleteFile($attrName);
				$hashFileName = $this->generateFilename($file->name);

				$this->owner->setAttribute($attrName, $hashFileName);
				$file->saveAs($this->getSavePath($hashFileName));
			}
			else {
				$this->owner->setAttribute($attrName, self::sanitize($this->owner->getAttribute($attrName)));
			}
		}

		return true;
	}

	public function beforeValidate($event)
	{
		foreach ($this->attributeNames as $attrName => $attrArray) {
			// if ($file = CUploadedFile::getInstance($this->owner, $attrName)) {
			if ($file = $this->getUploadedFileInstance($attrName)) {
				$hashFileName = $this->generateFilename($file->name);

				$this->owner->setAttribute($attrName, $hashFileName);
			}
		}
		return true;
	}

	/**
	 * Проверяет директорию файлов на наличие и на записываемость, создает или меняет права в случае необходимости
	 */
	private function checkSavePath()
	{
		$savePath = $this->getSavePath();
		if (is_dir($savePath)) {
			if (!is_writable($savePath)) {
				if (!chmod($savePath, 0775)) {
					throw new Exception("Не удалось сменить права на директорию {$this->getSaveUrl()}", 500);
				}
			}
		}
		else {
			if (!mkdir($savePath, 0775, true)) {
				throw new Exception("Не удалось создать директорию {$this->getSaveUrl()}", 500);
			}
		}
	}

	/**
	 * Возвращает путь к директории, в которой будут сохраняться файлы
	 * @param string $value
	 *
	 * @return string путь к директории, в которой сохраняем файлы
	 */
	public function getSavePath($value = '')
	{
		return implode(DIRECTORY_SEPARATOR, array(YiiBase::getPathOfAlias('webroot'), $this->savePath, $value));
	}

	/**
	 * Возвращает url к файлу
	 *
	 * @param string $value
	 *
	 * @return string
	 */
	public function getSaveUrl($value = '')
	{
		return implode('/', array(Yii::app()->getBaseUrl(), $this->savePath, $value));
	}

	/**
	 * Удаление файла
	 */
	public function deleteFile($attrName)
	{
		$attributeValue = self::sanitize($this->owner->getAttribute($attrName));
		$filePath = $this->getSavePath($attributeValue);

		//todo покрасивее сделать и разобраться с удалением
//		if (!is_writeable($filePath))
//			chmod($filePath, 0775);

		if (is_writeable($filePath) && is_file($filePath))
			unlink($filePath);
//		else
//			throw new Exception("Невозможно удалить файл $filePath");
	}

	/**
	 * Очищает строку с путем к файлу до одного названия файла
	 * @param $fileName
	 *
	 * @return mixed
	 */
	static private function sanitize($fileName)
	{
		$nameArray = explode('/', $fileName);

		return $nameArray[count($nameArray) - 1];
	}

	/**
	 * Хэширует имя файла (без расширения), сохраняет расширение нетронутым
	 * @param $fileName оригинальное имя файла
	 *
	 * @return string захэшированное имя файла
	 */
	static private function hashFileName($fileName)
	{
		$nameArray = explode('.', $fileName);
		$extension = $nameArray[count($nameArray) - 1];
		array_pop($nameArray);
		$nameWithoutExtension = uniqid(implode('.', $nameArray));

		return $nameWithoutExtension . '.' . $extension;
	}

	public function generateFilename($filename)
	{
		if ($this->saveOriginalFilename)
		{
			return $filename;
		}
		else
			return self::hashFileName($filename);
	}
	
	/**
	 * Позволяет выставлять значение атрибута модели, создает ImageHandler, если атрибут до этого был простым свойством
	 *
	 * @param $attrName имя атрибута
	 * @param $value путь к изображению или его имя
	 */
	protected function setAttribute($attrName, $value)
	{
		$attribute = $this->owner->getAttribute($attrName);
		$value = self::sanitize($value);
		$fullFileUrl = $this->getSaveUrl($value);
		
		if (!isset($this->attributeNames[$attrName]['image']) || $this->attributeNames[$attrName]['image']) {
			if ($attribute instanceof ImageHandler)
				$this->owner->{$attrName}->setImage($fullFileUrl);
			else {
				$image = new ImageHandler($this->thumbSizes, $fullFileUrl);
				$this->owner->setAttribute($attrName, $image);
			}
		}
		elseif (is_file($this->getSavePath($value)))
			$this->owner->setAttribute($attrName, $fullFileUrl);
	}

	public function afterFind($event)
	{
		foreach ($this->attributeNames as $attrName => $attrArray) {
			$this->setAttribute($attrName, $this->owner->getAttribute($attrName));
		}
	}

	public function afterSave($event)
	{
		foreach ($this->attributeNames as $attrName => $attrArray) {
			$this->setAttribute($attrName, $this->owner->getAttribute($attrName));
		}
	}

	public function beforeDelete($event)
	{
		$this->deleteAllFiles();
	}

	public function deleteAllFiles()
	{
		foreach ($this->attributeNames as $attrName => $attrArray) {
			$this->deleteFile($attrName);
		}
	}
}

/**
 * Позволяет заменить атрибут изображения классом, что позволяет вытягивать превьюшки.
 * Реализует метод __toString, чтобы возможно было получать из класса строку с ориг. файлом
 * Class ImageHandler
 */
class ImageHandler
{

	private $_image;
	private $_thumbSizes = array();
	private $_validFile;

	/**
	 * @param $thumbSizes
	 * @param null|string $value
	 */
	public function __construct($thumbSizes, $value = null)
	{
		if ($value !== null) {
			$this->_image = $value;
		}

		if ($thumbSizes !== null) {
			$this->_thumbSizes = $thumbSizes;
		}

		if ($this->isFile($value)) {
			$this->_validFile = true;
			$this->generateThumbs();
		}
	}

	protected function isFile($fileName)
	{
		if (is_file($_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . $fileName))
			return true;
		else
			return false;
	}

	/**
	 * Генерирует изображение/изображения всех типов
	 * @param null|string $type запрашиваемый тип превью
	 *
	 * @return bool
	 */
	protected function generateThumbs($type = null)
	{
		if (count($this->_thumbSizes) > 0 && $this->_validFile) {
			if ($type !== null) {
				$this->processImage($type);
			}
			else {
				foreach ($this->_thumbSizes as $thumbType => $thumbSize) {
					$this->processImage($thumbType);
				}
			}
		}

		return true;
	}

	/**
	 * Обработка изображения
	 * @param string $thumbType запрашиваемый тип превью
	 */
	protected function processImage($thumbType)
	{
		$filePath = $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . $this->_image;
		$thumbPath = $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . $this->getThumbFileName($thumbType);
		$maxWidth = $this->_thumbSizes[$thumbType][0];
		$maxHeight = $this->_thumbSizes[$thumbType][1];

		if (!is_file($thumbPath)) {
			
			/* Imagick */
			
			// $image = new Imagick($filePath);
			// if ($maxWidth == 0 || $maxHeight == 0)
				// $image->thumbnailimage($maxWidth, $maxHeight);
			// else
				// $image->thumbnailimage($width, $maxHeight, true);
			// $image->writeImage($maxWidth);
			// $image->destroy(); 
			
			/* gd */
			
			list($sourceWidth, $sourceHeight, $sourceType) = getimagesize($filePath);
			
			switch ($sourceType) 
			{
				case IMAGETYPE_GIF:
					$source = imagecreatefromgif($filePath);
					break;
				case IMAGETYPE_JPEG:
					$source = imagecreatefromjpeg($filePath);
					break;
				case IMAGETYPE_PNG:
					$source = imagecreatefrompng($filePath);
					break;
			}
			
			if ($source === false) 
			{
				return false;
			}
			
			/* TODO: написать crop или заменить класс на 
			   все умеющий ImageHandler:
			   https://github.com/tokolist/yii-components/tree/master/protected/extensions/yiicomp/components/ImageHandler
			*/
			// var_dump(
			// 	$maxWidth,
			// 	$maxHeight,
			// 	$sourceWidth,
			// 	$sourceHeight
			// );
			if ($maxHeight == 0 || $maxWidth == 0)
			{
				if ($maxWidth == 0 && $sourceHeight > $maxHeight)
				{
					$thumbnailHeight = $maxHeight;
					$thumbnailWidth = (int) ($sourceWidth * ($maxHeight / $sourceHeight));
				}
				elseif ($maxHeight == 0 && $sourceWidth > $maxWidth)
				{
					$thumbnailWidth = $maxWidth;
					$thumbnailHeight = (int) ($sourceHeight * ($maxWidth / $sourceWidth));
				}
				else
				{
					$thumbnailWidth = $sourceWidth;
					$thumbnailHeight = $sourceHeight;
				}
			}
			elseif ($sourceHeight > $maxHeight || $sourceWidth > $maxWidth)
			{
				$sourceAspectRatio = $sourceWidth / $sourceHeight;
				$thumbnailAspectRatio = $maxWidth / $maxHeight;
				
				if ($thumbnailAspectRatio > $sourceAspectRatio) 
				{
					$thumbnailHeight = $maxHeight;
					$thumbnailWidth = (int) ($sourceWidth * ($maxHeight / $sourceHeight));
				}
				else 
				{
					$thumbnailWidth = $maxWidth;
					$thumbnailHeight = (int) ($sourceHeight * ($maxWidth / $sourceWidth));
				}
			}
			else
			{
				$thumbnailWidth = $sourceWidth;
				$thumbnailHeight = $sourceHeight;
			}

			$thumbnail = imagecreatetruecolor($thumbnailWidth, $thumbnailHeight);
			
			imagealphablending($thumbnail, false);
			imagesavealpha($thumbnail, true);
			
			imagecopyresampled($thumbnail, $source, 0, 0, 0, 0, $thumbnailWidth, $thumbnailHeight, $sourceWidth, $sourceHeight);
						
			switch ($sourceType) 
			{
				case IMAGETYPE_GIF:
					imagegif($thumbnail, $thumbPath);
					break;
				case IMAGETYPE_JPEG:
					imagejpeg($thumbnail, $thumbPath, 90);
					break;
				case IMAGETYPE_PNG:
					imagepng($thumbnail, $thumbPath, 0);
					break;
			}
			
			imagedestroy($source);
			imagedestroy($thumbnail);
		}
		
	}

	/**
	 * @param $type запрашиваемый тип превью
	 *
	 * @return string путь или имя файла превью
	 */
	protected function getThumbFileName($type)
	{
		$fileNameArray = explode('/', $this->_image);
		$fileNameArray[count($fileNameArray) - 1] = $type . '_' . $fileNameArray[count($fileNameArray) - 1];

		$thumbName = implode('/', $fileNameArray);

		return $thumbName;
	}

	public function setImage($value)
	{
		$this->checkFile($value);
		$this->_image = $value;
	}

	protected function checkFile($fileName)
	{
		if (is_file($_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . $fileName))
			$this->_validFile = true;
		else
			$this->_validFile = false;
	}

	/**
	 * Позволяет приводить класс к строке
	 * @return null|string
	 */
	public function __toString()
	{
		return $this->_validFile ? $this->_image : '';
	}

	/**
	 * Основной метод, используемый от атрибута: $model->image->thumb() or $model->image->thumb('thumb_type')
	 *
	 * @param string $type запрашиваемый тип превью
	 *
	 * @return string путь к превью
	 * @throws Exception
	 */
	public function thumb($type = 'default')
	{
		if (isset($this->_thumbSizes[$type])) {
			return $this->_validFile ? $this->getThumb($type) : null;
		}
		else
			throw new Exception('Запрашиваемого типа превью не существует, возможно вы не указали его в thumbSizes модели.');
	}

	/**
	 * Если запрашиваемого файла не существует, генерирует превью нужного типа
	 * @param $type запрашиваемый тип превью
	 *
	 * @return string путь к превью
	 */
	protected function getThumb($type)
	{
		$fileName = $this->getThumbFileName($type);
		$this->generateThumbs($type);

		return $fileName;
	}
}