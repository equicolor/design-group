<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8" />
	<title>Design Group</title>

	<base href="<?= Yii::app()->getBaseUrl(true); ?>/" />

	<link rel="stylesheet" href="style.css" />

	<!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> -->
	<!-- 33 KB -->

	<link  href="http://cdnjs.cloudflare.com/ajax/libs/fotorama/4.5.1/fotorama.css" rel="stylesheet"> <!-- 3 KB -->
	<script src="http://cdnjs.cloudflare.com/ajax/libs/fotorama/4.5.1/fotorama.js"></script> <!-- 16 KB -->
</head>

<body>
	<div class="page-wrapper">
		<div class="b-header">
			<?php if (Yii::app()->controller->id == 'site' && Yii::app()->controller->action->id == 'index'): ?>
				<i class="s s--logo"></i>
			<?php else: ?>
				<a href="<?= Yii::app()->getBaseUrl(true); ?>"><i class="s s--logo"></i></a>
			<?php endif; ?>
			<div class="b-header--contacts">
				<span><?= Config::value('phone'); ?></span>
				<br />
				<a href="<?= Config::value('email'); ?>"><?= Config::value('email'); ?></a>
			</div>
		</div>

		<div class="b-nav">
			<div class="b-nav--inner">
				<?php $this->widget('MainMenu',array(
					'items'=>array(
						array('label'=>'Работы', 'url'=>array('project/index'), 'active' => Yii::app()->controller->id == 'project'),
						array('label'=>'Услуги', 'url'=>array('service/view')),
						array('label'=>'Клиенты', 'url'=>array('customer/index')),
						array('label'=>'О Design Group', 'url'=>array('site/about')),
						// array('label' => 'Админка', 'url' => array('admin/main/index'), 'visible' => !Yii::app()->user->isGuest),
						// array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
						// array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)
					),
					'activeCssClass' => 'active_page',
					'htmlOptions' => array(
						'class' => 'b-navmenu',
					),
				)); ?>
				<!--<span class="b-language">
					<a class="active" href="#">ru</a>
					<span>|</span>
					<a href="#">en</a>
				</span>-->
			</div>
		</div>

		<?php echo $content; ?>

		<div class="page-buffer"></div>
	</div>
	
	<div class="b-footer">
		<div class="b-footer--inner">
			<i class="s s--logo_footer"></i>

			<span class="b-footer__date">© 2008—2014</span>
			<span class="b-footer__name">JM Design Group</span>

			<div class="b-header--contacts footer">
				<span><?= Config::value('phone'); ?></span>
				<br />
				<a href="<?= Config::value('email'); ?>"><?= Config::value('email'); ?></a>
			</div>
		</div>
	</div>

</body>

</html>