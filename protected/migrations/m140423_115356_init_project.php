<?php

class m140423_115356_init_project extends CDbMigration
{
	public function up()
	{
		$this->createTable('project', array(
			'id' => 'pk',
			'customerId' => 'int(11) NOT NULL',
			'categoryId' => 'int(11) NOT NULL',
			'name' => 'varchar(256) NOT NULL',
			'content' => 'MEDIUMTEXT NOT NULL',
			'jsonTeam' => 'text',
		));
	}

	public function down()
	{
		$this->dropTable('project');
	}
}