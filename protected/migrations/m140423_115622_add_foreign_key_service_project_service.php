<?php

class m140423_115622_add_foreign_key_service_project_service extends CDbMigration
{
	public function up()
	{
		$this->addForeignKey(
			'serviceHasManyProjects',
			'serviceProject',
			'serviceId',
			'service',
			'id',
			'cascade',
			'cascade'
		);
	}

	public function down()
	{
		$this->dropForeignKey('serviceHasManyProjects', 'serviceProject');
	}
}