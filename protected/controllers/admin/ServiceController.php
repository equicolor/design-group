<?php

class ServiceController extends AdminController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */

	public $defaultAction = 'admin';
	
	public function actions()
	{
		return array(
			'sort' => array(
				'class' => 'SortAction',
				'className' => 'Service',
				'positionAttribute' => 'position',
				// 'params' => ['product_id'],
				// 'criteriaCallback' => function ($image_id, $params) {
				// 	return [
				// 		'condition' => 't.product_id = :product_id AND t.image_id = :image_id',
				// 		'params' => [
				// 			'product_id' => $params['product_id'],
				// 			'image_id' => $image_id,
				// 		],
				// 	];
				// },
			),
		);
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model = ServiceForm::create();

		if ($model->issetAttributes($_POST))
		{
			$model->safeAssign($_POST);

			if ($model->save())
			{
				// $this->redirect(array('view','id'=>$model->id));
				
				Yii::app()->user->setFlash('result', 'Изменения успешно сохранены');
				$this->redirect(array('admin'));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$service = $this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		$model = ServiceForm::create(array(
			'service' => $service,
		));

		if ($model->issetAttributes($_POST))
		{
			$model->safeAssign($_POST);

			if (!isset($_POST['ServiceForm']['projects']))
			{
				$model->projects = array();
			}

			if ($model->save())
			{
				// $this->redirect(array('view','id'=>$model->id));
				
				Yii::app()->user->setFlash('result', 'Изменения успешно сохранены');
				$this->redirect(array('admin'));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Service('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Service']))
			$model->attributes=$_GET['Service'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Service the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Service::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Service $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='service-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
