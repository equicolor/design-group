<?php
/* @var $this ServiceController */
/* @var $model Service */

$this->breadcrumbs=array(
	'Услуги'=>array('admin'),
	$model->service->name,
	'Редактирование',
);

$this->menu=array(
	array('label'=>'Создать услугу', 'url'=>array('create')),
	array('label'=>'Список услуг', 'url'=>array('admin')),
);

$this->pageTitle = "Редактирование услуги {$model->service->name}";
?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>