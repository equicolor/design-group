<?php

Yii::setPathOfAlias('widgets', 'protected/widgets');
Yii::setPathOfAlias('bootstrap', dirname(__FILE__).'/../extensions/bootstrap');

if (file_exists($paramsFilename = __DIR__ . DIRECTORY_SEPARATOR . 'params.php'))
{
	$params = require_once($paramsFilename);
}
else
{
	$params = array();
}

if (file_exists($dbConfigFilename = __DIR__ . DIRECTORY_SEPARATOR . '_db.php'))
{
	$dbConfig = require_once($dbConfigFilename);
}
else
{
	$dbConfig = array();
}

return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Design Group',

	'language' => 'ru',	
	'sourceLanguage' => 'ru',

	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.models.formModels.*',
		'application.components.*',
		'application.widgets.*',
		'application.helpers.*',
		'bootstrap.helpers.TbHtml',
	),

	'modules'=>array(
		// uncomment the following to enable the Gii tool
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'root',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
		),
	),

	'components'=>array(
		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
		),		
		'urlManager'=>array(
			'showScriptName' => false,
			'urlFormat'=>'path',
			'rules'=>array(
				'projects/category/<urlName:\w+>' => 'project/index',
				'projects/<urlName:[\w-]+>' => 'project/view',
				'projects' => 'project/index',

				'customers/<urlName:[\w-]+>' => 'customer/view',
				'customers' => 'customer/index',

				'services/<urlName:[\w-]+>' => 'service/view',
				'services' => 'service/view',

				'about' => 'site/about',
				'service' => 'service/index',
				'admin' => 'admin/main',
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),
		'db' => CMap::mergeArray(
			array(
				'connectionString' => 'mysql:host=localhost;dbname=design_group;port=8889;unix_socket=/Applications/MAMP/tmp/mysql/mysql.sock',
				'emulatePrepare' => true,
				'username' => 'root',
				'password' => 'root',
				'charset' => 'utf8',				
			),
			$dbConfig
		),
		'errorHandler'=>array(
			'errorAction'=>'site/error',
		),
		'bootstrap' => array(
			'class' => 'bootstrap.components.TbApi',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),
	),
	'params' => CMap::mergeArray(
		array(
			//default params there
		),
		$params
	),
);