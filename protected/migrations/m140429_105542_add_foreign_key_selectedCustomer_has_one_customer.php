<?php

class m140429_105542_add_foreign_key_selectedCustomer_has_one_customer extends CDbMigration
{
	public function up()
	{
		$this->addForeignKey(
			'selectedCustomerHasOneCustomer',
			'selectedCustomer',
			'id',
			'customer',
			'id',
			'cascade',
			'cascade'
		);
	}

	public function down()
	{
		$this->dropForeignKey('selectedCustomerHasOneCustomer', 'selectedCustomer');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}