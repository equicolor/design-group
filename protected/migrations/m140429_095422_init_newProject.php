<?php

class m140429_095422_init_newProject extends CDbMigration
{
	public function up()
	{
		$this->createTable('newProject', array(
			'id' => 'int(11) NOT NULL',
		));
	}

	public function down()
	{
		$this->dropTable('newProject');
	}
}